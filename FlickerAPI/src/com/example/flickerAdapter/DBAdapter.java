package com.example.flickerAdapter;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.flickerClasses.FlickerConstants;
import com.example.flickerClasses.FlickerItem;

public class DBAdapter implements FlickerConstants {

	private static final String FLICKER_TABLE = "flickeritem";

	private static final String TAG = "DBAdapter";

	private static final String DATABASE_NAME = "flicker";
	private static final int DATABASE_VERSION = 1;

	// "create table "+CATEGORY_TABLE_NAME+" (category TEXT,isSelected BOOLEAN,category_id TEXT)";

	private static final String FLICKER_TABLE_CREATE = "create table "
			+ FLICKER_TABLE + "(  " + FlickerConstants.TITLE + " text, "
			+ FlickerConstants.LINK + " text, " + FlickerConstants.MEDIA
			+ " text, " + FlickerConstants.PUBLISHED + " text, "
			+ FlickerConstants.AUTHOR + " text, " + FlickerConstants.AUTHOR_ID
			+ " text, " + FlickerConstants.TAGS + " text, "
			+ FlickerConstants.DATE_TAKEN + " text, "
			+ FlickerConstants.DESCRIPTION + " text, "
			+ FlickerConstants.PRIMARY_KEY_ID
			+ " integer primary key autoincrement);";

	private final Context context;
	private DatabaseHelper DBHelper;
	public SQLiteDatabase db;

	public DBAdapter(Context ctx) {
		this.context = ctx;
		DBHelper = new DatabaseHelper(context);
	}

	private static class DatabaseHelper extends SQLiteOpenHelper {
		DatabaseHelper(Context context) {

			super(context, DATABASE_NAME, null, DATABASE_VERSION);

		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			try {
				Log.d("Create Table", FLICKER_TABLE_CREATE);
				db.execSQL(FLICKER_TABLE_CREATE);
				// db.execSQL(GAP_DATE_TABLE_CREATE);

			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
					+ newVersion + ", which will destroy all old data");
			db.execSQL("DROP TABLE IF EXISTS " + FLICKER_TABLE);
			onCreate(db);
		}
	}

	// ---opens the database---
	public DBAdapter open() throws SQLException {
		db = DBHelper.getWritableDatabase();
		return this;
	}

	// ---closes the database---
	public void close() {
		DBHelper.close();
	}

	public void deleteAllItems() {
		db.execSQL("delete  from " + FLICKER_TABLE);
	}

	// ---insert a contact into the database---
	public long insertFlickerItem(FlickerItem item) {
		ContentValues initialValues = new ContentValues();

		Log.e("Title of Item While Inserting:", item.getTitle());
		initialValues.put(FlickerConstants.TITLE, item.getTitle());
		initialValues.put(FlickerConstants.LINK, item.getLink());
		initialValues.put(FlickerConstants.AUTHOR, item.getAuthor());
		initialValues.put(FlickerConstants.AUTHOR_ID, item.getAuthorId());
		initialValues.put(FlickerConstants.DATE_TAKEN, item.getDateTaken());
		initialValues.put(FlickerConstants.DESCRIPTION, item.getDescription());
		initialValues.put(FlickerConstants.MEDIA, item.getImageLink());
		initialValues.put(FlickerConstants.PUBLISHED, item.getPublishDate());
		initialValues.put(FlickerConstants.TAGS, item.getTags());

		return db.insert(FLICKER_TABLE, null, initialValues);
	}

	public ArrayList<FlickerItem> getAllFlickerItems() {
		ArrayList<FlickerItem> itemsList = new ArrayList<FlickerItem>();
		Cursor c = db.rawQuery("select * from " + FLICKER_TABLE, null);

		if (c.getCount() > 0) {
			c.moveToFirst();
			for (int i = 0; i < c.getCount(); i++) {
				FlickerItem item = new FlickerItem();

				item.setTitle(c.getString(c.getColumnIndex(TITLE)));
				item.setAuthorId(c.getString(c.getColumnIndex(AUTHOR_ID)));
				item.setAuthor(c.getString(c.getColumnIndex(AUTHOR)));
				item.setLink(c.getString(c.getColumnIndex(LINK)));
				item.setPublishDate(c.getString(c.getColumnIndex(PUBLISHED)));
				item.setTags(c.getString(c.getColumnIndex(TAGS)));
				item.setDescription(c.getString(c.getColumnIndex(DESCRIPTION)));
				item.setDateTaken(c.getString(c.getColumnIndex(DATE_TAKEN)));
				item.setImageLink(c.getString(c.getColumnIndex(MEDIA)));
				
				Log.e("Image Link While Fetching", item.getImageLink());
				itemsList.add(item);
				c.moveToNext();
			}
		}
		return itemsList;
	}

	// ---retrieves a particular joke according to category---
	/*
	 * public Cursor getJokesCategoryWise(String category) throws SQLException {
	 * 
	 * Cursor mCursor = db.query(false, DATABASE_TABLE, new String[] {
	 * FlickerConstants.item_id, FlickerConstants.item_image_url,
	 * FlickerConstants.item_text, FlickerConstants.item_title,
	 * FlickerConstants.item_isRead }, FlickerConstants.JOKE_CATEGORY + "=[" +
	 * category+"]" , null, null, null, null, null);
	 * 
	 * 
	 * return db.rawQuery("select * from " + DATABASE_TABLE + " where " +
	 * FlickerConstants.JOKE_CATEGORY + " = ?", new String[] { category });
	 * 
	 * }
	 */

}
