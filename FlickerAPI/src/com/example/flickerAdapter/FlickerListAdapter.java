package com.example.flickerAdapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.flickerClasses.FlickerItem;
import com.example.flickerapi.R;
import com.example.imagedownloadutil.DownLoadImageLoader;

public class FlickerListAdapter extends ArrayAdapter {

	Context ctx;
	ArrayList<FlickerItem> itemList;
	ViewHolder holder;
	DownLoadImageLoader loader;

	public FlickerListAdapter(Context context, int resource, List objects) {
		super(context, resource, objects);
		itemList = (ArrayList<FlickerItem>) objects;
		ctx = context;
		loader = new DownLoadImageLoader(ctx);
		// TODO Auto-generated constructor stub
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		holder = new ViewHolder();
		FlickerItem item = itemList.get(position);
		String url = "";
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) ctx
					.getSystemService(ctx.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.listrow2, null);
			holder.image = (ImageView) convertView.findViewById(R.id.flickerImage);
			holder.text = (TextView) convertView.findViewById(R.id.text);
			holder.title = (TextView) convertView.findViewById(R.id.title);
			holder.dateText = (TextView) convertView
					.findViewById(R.id.jokeDateTextView);

			convertView.setTag(holder);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.dateText.setText(item.getDateTaken());
		holder.text.setText("Author:"+item.getAuthor());
		holder.title.setText("Title :"+item.getTitle());
		
		loader.DisplayImage(item.getImageLink(), holder.image);
		

		return convertView;
	}

	private class ViewHolder {
		ImageView image;
		TextView title;
		TextView text;
		TextView dateText;
	}
}
