package com.example.flickerapi;

import java.util.ArrayList;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.example.flickerAdapter.DBAdapter;
import com.example.flickerAdapter.FlickerListAdapter;
import com.example.flickerClasses.FlickerConstants;
import com.example.flickerClasses.FlickerItem;

public class FlickerListViewActivity extends FlickerActivity implements
		OnItemClickListener, OnClickListener {
	ListView flickerListView;
	FlickerListAdapter listAdapter;
	ArrayList<FlickerItem> itemList;
	Button downloadData;
	ProgressDialog dialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.list_view);
		downloadData = (Button) findViewById(R.id.downLoadListData);
		downloadData.setOnClickListener(this);
		flickerListView = (ListView) findViewById(R.id.flickerListView);
		listAdapter = new FlickerListAdapter(this, R.layout.listrow2,
				getFlickerItemsList());
		flickerListView.setAdapter(listAdapter);
		flickerListView.setOnItemClickListener(this);
	}

	@Override
	public void onTaskEnd(Object result) {
		// TODO Auto-generated method stub
		if(result == null){
			Toast.makeText(this, "Doawnloading Failed", Toast.LENGTH_LONG).show();
			return;
		}
		//itemList = getFlickerItemsList();
		listAdapter = new FlickerListAdapter(this, R.layout.listrow2,
				getFlickerItemsList());
		flickerListView.setAdapter(listAdapter);
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		outState.putSerializable(FlickerConstants.FLICKER_ITEM_BUNDLE_KEY, itemList);
		super.onSaveInstanceState(outState);
	}

	private ArrayList<FlickerItem> getFlickerItemsList() {
		DBAdapter adapter = new DBAdapter(this);
		adapter.open();
		itemList = adapter.getAllFlickerItems();
		adapter.close();
		Log.e("ArrayList Count :", itemList.size()+"");
		return itemList;

	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int index, long arg3) {
		// TODO Auto-generated method stub
		Bundle bundle = new Bundle();
		bundle.putSerializable(FlickerConstants.FLICKER_ITEM_BUNDLE_KEY,
				itemList.get(index));
		Intent i = new Intent(this, FlickerItemDetailView.class);
		i.putExtras(bundle);
		startActivity(i);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.downLoadListData:
			downloadData();
			break;
		}
	}

	

}
