package com.example.flickerapi;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;

import com.example.flickerClasses.TaskInterface;
import com.example.imagedownloadutil.DownLoadImageLoader;
import com.example.imagedownloadutil.DownloadJsonTask;

public class FlickerActivity extends Activity implements TaskInterface {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.flicker, menu);
		return true;
	}

	public void downloadData() {
		Log.e("Download Data Method", "Flicker Activity");
		DownloadJsonTask task = new DownloadJsonTask(this, this);
		task.execute();
	}

	@Override
	public void onTaskBegin() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTaskEnd(Object result) {
		// TODO Auto-generated method stub

	}

	@Override
	public void taskInBackground(String jsonString) {
		// TODO Auto-generated method stub

	}

}
