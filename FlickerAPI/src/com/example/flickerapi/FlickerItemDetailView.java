package com.example.flickerapi;

import android.os.Bundle;
import android.text.Html;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.flickerClasses.FlickerConstants;
import com.example.flickerClasses.FlickerItem;
import com.example.imagedownloadutil.DownLoadImageLoader;

public class FlickerItemDetailView extends FlickerActivity {
	FlickerItem item = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.flicker_detail_item);

		if (savedInstanceState != null) {
			item = (FlickerItem) savedInstanceState
					.getSerializable(FlickerConstants.FLICKER_ITEM_BUNDLE_KEY);
		} else {
			Bundle bundle = getIntent().getExtras();
			item = (FlickerItem) bundle
					.getSerializable(FlickerConstants.FLICKER_ITEM_BUNDLE_KEY);

		}
		initViews(item);

	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		outState.putSerializable(FlickerConstants.FLICKER_ITEM_BUNDLE_KEY, item);
		super.onSaveInstanceState(outState);
	}

	private void initViews(FlickerItem item) {
		if (item != null) {
			((TextView) findViewById(R.id.author)).setText("Author: "
					+ item.getAuthor());
			((TextView) findViewById(R.id.authorID)).setText("Author Id: "
					+ item.getAuthorId());
			((TextView) findViewById(R.id.item_title)).setText(Html
					.fromHtml("Title: " + item.getTitle()));
			((TextView) findViewById(R.id.date_taken)).setText("Date Taken: "
					+ item.getDateTaken());
			((TextView) findViewById(R.id.description)).setText(Html
					.fromHtml(("Description: " + item.getDescription())));
			((TextView) findViewById(R.id.publishDate))
					.setText("publish Date: " + item.getPublishDate());
			((TextView) findViewById(R.id.link)).setText(Html.fromHtml("Link: "
					+ item.getLink()));
			ImageView imageView = (ImageView) findViewById(R.id.media_image);
			DownLoadImageLoader loader = new DownLoadImageLoader(this);
			loader.DisplayImage(item.getImageLink(), imageView);
		}

	}
}
