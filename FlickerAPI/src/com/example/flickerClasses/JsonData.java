package com.example.flickerClasses;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

import com.example.flickerAdapter.DBAdapter;

public class JsonData implements FlickerConstants {

	private JSONObject json;

	public Object getJsonData(Context ctx) {
		DBAdapter adapter = new DBAdapter(ctx);
		adapter.open();
		JSONParser parser = new JSONParser();
		String jsonString = parser
				.getJSONFromUrl(FlickerConstants.FLICKER_API_URL);
		if (jsonString == null) {
			return null;
		}
		jsonString = jsonString.substring(1, jsonString.length());
		try {
			JSONObject obj = new JSONObject(jsonString);

			JSONArray array = obj.getJSONArray(ITEMS);

			if (array != null) {
				adapter.deleteAllItems();
				for (int i = 0; i < array.length(); i++) {
					FlickerItem item = new FlickerItem();
					JSONObject itemJson = array.getJSONObject(i);
					

					
					item.setTitle(itemJson.getString(TITLE));
					item.setAuthorId(itemJson.getString(AUTHOR_ID));
					item.setAuthor(itemJson.getString(AUTHOR));
					item.setLink(itemJson.getString(LINK));
					item.setPublishDate(itemJson.getString(PUBLISHED));
					item.setTags(itemJson.getString(TAGS));
					item.setDescription(itemJson.getString(DESCRIPTION));
					item.setDateTaken(itemJson.getString(DATE_TAKEN));

					// to get image link from media object
					String media = itemJson.getString(MEDIA);
					Log.e("Json String Media is:", itemJson.getString(MEDIA));
					JSONObject mediaObject = new JSONObject(media);
					Log.e("Json String Media is:", mediaObject.getString(M));
					item.setImageLink(mediaObject.getString(M));
					// =========================================
					// Insert Data into database
					long ins = adapter.insertFlickerItem(item);
					Log.e("Item Inserteed or not:", ins+"");

				}
				return true;
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		adapter.close();
		return null;
	}
}
