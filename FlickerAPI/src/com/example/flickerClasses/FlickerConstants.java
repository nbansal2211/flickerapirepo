package com.example.flickerClasses;

public interface FlickerConstants {

	String FLICKER_API_URL = "http://api.flickr.com/services/feeds/photos_public.gne?jsoncallback=?&format=json";
	String ITEMS = "items";
	String TITLE = "title";
	String LINK = "link";
	String MEDIA = "media";
	String M = "m";
	String DATE_TAKEN = "date_taken";
	String AUTHOR = "author";
	String AUTHOR_ID = "author_id";
	String TAGS = "tags";
	String PUBLISHED = "published";
	String PRIMARY_KEY_ID = "_id";
	String DESCRIPTION = "description";
	
	String FLICKER_ITEM_BUNDLE_KEY = "flickerItemKey";
	
}
