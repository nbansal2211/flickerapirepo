package com.example.flickerClasses;

public interface TaskInterface {
	
	public void onTaskBegin();
	public void onTaskEnd(Object result);
	public void taskInBackground(String jsonString);

}
