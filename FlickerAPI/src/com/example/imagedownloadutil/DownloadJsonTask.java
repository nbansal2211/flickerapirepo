package com.example.imagedownloadutil;

import com.example.flickerClasses.JsonData;
import com.example.flickerClasses.TaskInterface;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

public class DownloadJsonTask extends AsyncTask {
	TaskInterface taskRef;
	Context ctx;
	ProgressDialog dialog;

	public DownloadJsonTask(Context ctx, TaskInterface ref) {
		this.ctx = ctx;
		taskRef = ref;
	}
	
	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		dialog = new ProgressDialog(ctx);
		dialog.setTitle("Please wait");
		dialog.setMessage("Downloading Data...");
		dialog.setCancelable(false);
		dialog.show();
	}

	@Override
	protected Object doInBackground(Object... arg0) {
		// TODO Auto-generated method stub
		JsonData data = new JsonData();
		
		// taskRef.taskInBackground(jsonString);
		return data.getJsonData(this.ctx);
	}

	@Override
	protected void onPostExecute(Object result) {
		// TODO Auto-generated method stub
		dialog.dismiss();
		taskRef.onTaskEnd(result);
	}

}
